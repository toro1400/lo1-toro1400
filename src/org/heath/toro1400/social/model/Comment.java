package org.heath.toro1400.social.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name="COMMENTS")
public class Comment implements Serializable {
        private static final long serialVersionUID = 1L;
        
        @Id
        @GeneratedValue
        private long id;
        
        @ManyToOne(cascade=CascadeType.ALL)
        @JoinColumn(name="EventID")
        private Event event;
        
        @ManyToOne(cascade=CascadeType.ALL)
        @JoinColumn(name="UserID")
        private User author;
        
        private String commentText;
        private LocalDateTime time;
        private LocalDateTime lastUpdate;
        
        public Comment() {}
        
        public Comment(Event event, User author, String commentText) {
                this(event, author, commentText, LocalDateTime.now());
        }
        
        public Comment(Event event, User author, String commentText, LocalDateTime time) {
                this.event = event;
                this.author = author;
                this.commentText = commentText;
                this.time = time;
        }
        
        @PrePersist
        private void prePersist() {
                this.lastUpdate = LocalDateTime.now();
        }
        
        @PreUpdate
        private void preUpdate() {
                this.lastUpdate = LocalDateTime.now();
        }
        
        // Getters
        public long getId() {
                return id;
        }
        
        public Event getEvent() {
                return event;
        }
        
        public User getAuthor() {
                return author;
        }
        
        public String getComment() {
                return commentText;
        }
        
        public LocalDateTime getTime() {
                return time;
        }
        
        public LocalDateTime getLastUpdate() {
                return lastUpdate;
        }
        
        // Setters
        
        public void setId(long id) {
                this.id = id;
        }
        
        public void setEvent(Event event) {
                this.event = event;
        }
        
        public void setAuthor(User author) {
                this.author = author;
        }
        
        public void setTime(LocalDateTime time) {
                this.time = time;
        }
        
        public void setLastUpdate(LocalDateTime lastUpdate) {
                this.lastUpdate = lastUpdate;
        }
}
