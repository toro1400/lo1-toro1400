package org.heath.toro1400.social.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name="EVENTS")
public class Event implements Serializable {
        private final static long serialVersionUID = 1L;
        
        @Id
        @GeneratedValue
        private long id;
        private String title;
        private String city;
        private String content;
        private LocalDateTime startTime;
        private LocalDateTime endTime;
        private LocalDateTime lastUpdate;
        
        @OneToMany(mappedBy="event", cascade=CascadeType.ALL)
        private Set<Comment> comments;
        
        @ManyToMany
        @JoinTable(
                        name="ORGANIZERS",
                        joinColumns=@JoinColumn(name="EventID"),
                        inverseJoinColumns=@JoinColumn(name="UserID")
                        )
        private Set<User> organizers;
        
        public Event() {}
        
        public Event(String title, String city, String content, LocalDateTime startTime, LocalDateTime endTime, Set<User> organizers) {
                this.title = title;
                this.city = city;
                this.content = content;
                this.startTime = startTime;
                this.endTime = endTime;
                this.comments = new HashSet<Comment>();
                this.organizers = organizers;
        }
        
        @PrePersist
        private void prePersist() {
                this.lastUpdate = LocalDateTime.now(); 
        }
        
        @PreUpdate
        private void preUpdate() {
                this.lastUpdate = LocalDateTime.now();
        }
        
        // Getters
        public long getId() {
                return id;
        }
        
        public String getTitle() {
                return title;
        }
        
        public String getCity() {
                return city;
        }
        
        public String getContent() {
                return content;
        }
        
        public LocalDateTime getStartTime() {
                return startTime;
        }
        
        public LocalDateTime getEndTime() {
                return endTime;
        }
        
        public LocalDateTime getLastUpdate() {
                return lastUpdate;
        }
        
        public Set<Comment> getComments() {
                return comments;
        }
        
        public Set<User> getOrganizers() {
                return organizers;
        }
        
        // Setters
        public void setId(long id) {
                this.id = id;
        }
        
        public void setTitle(String title) {
                this.title = title;
        }
        
        public void setCity(String city) {
                this.city = city;
        }
        
        public void setContent(String content) {
                this.content = content;
        }
        
        public void setStartTime(LocalDateTime startTime) {
                this.startTime = startTime;
        }
        
        public void setEndTime(LocalDateTime endTime) {
                this.endTime = endTime;
        }
        
        public void setLastUpdate(LocalDateTime lastUpdate) {
                this.lastUpdate = lastUpdate;
        }
        
        public void setComments(Set<Comment> comments) {
                this.comments = comments;
        }
        
        public void setOrganizers(Set<User> organizers) {
                this.organizers = organizers;
        }
}
