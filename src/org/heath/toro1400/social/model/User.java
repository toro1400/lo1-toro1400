package org.heath.toro1400.social.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="USERS")
public class User implements Serializable {
        private static final long serialVersionUID = 1L;
        
        @Id
        @GeneratedValue
        private long id;
        
        private String firstName;
        private String lastName;
        private String email;
        
        @OneToMany(mappedBy="author", cascade=CascadeType.ALL)
        private Set<Comment> comments;
        
        public User() {}
        
        public User(String firstName, String lastName, String email) {
                this.firstName = firstName;
                this.lastName = lastName;
                this.email = email;
                this.comments = new HashSet<Comment>();
        }
        
        // Getters
        public long getId() {
                return id;
        }
        
        public String getFirstName() {
                return firstName;
        }
        
        public String getLastName() {
                return lastName;
        }
        
        public String getEmail() {
                return email;
        }
        
        public Set<Comment> getComments() {
                return comments;
        }
        
        // Setters
        public void setId(long id) {
                this.id = id;
        }
        
        public void setFirstName(String firstName) {
                this.firstName = firstName;
        }
        
        public void setLastName(String lastName) {
                this.lastName = lastName;
        }
        
        public void setEmail(String email) {
                this.email = email;
        }
        
        public void setComments(Set<Comment> comments) {
                this.comments = comments;
        }
}
