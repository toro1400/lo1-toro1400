package org.heath.toro1400.social;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Set;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.heath.toro1400.social.model.Comment;
import org.heath.toro1400.social.model.Event;
import org.heath.toro1400.social.model.User;

public class DatabasePopulator {

	private static final String defaultURL = "https://bitbucket.org/javaee2016/javaee16/raw/master/material/events.txt";
	
	private EntityManagerFactory entityManagerFactory;
	
	public static void main(String[] args) {
		System.out.println("A");
		EntityManagerFactory emfer = Persistence.createEntityManagerFactory("social");
		DatabasePopulator databasePopulator = new DatabasePopulator(emfer);
		System.out.println("B");
		if (args.length == 1) {
			databasePopulator.populate(args[0]);
		} else {
			databasePopulator.populate(defaultURL);
		}
		if (emfer != null) {
			emfer.close();
		}
	}
		
	public DatabasePopulator(EntityManagerFactory emf) {
		this.entityManagerFactory = emf;
	}
	
	public void populate(String inputURLString) {
		InputStream inputStream = null;
		Parser parser = null;
		
		try {
			URL url = new URL(inputURLString);
			inputStream = url.openStream();
			parser = new Parser(inputStream);
			parser.parse();
			
			Set<User> users = parser.getUsers();
			Set<Event> events = parser.getEvents();
			Set<Comment> comments = parser.getComments();
			
			SocialDAO dao = new SocialDAO(entityManagerFactory);
			dao.persistAll(users, events, comments);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {}
			}
		}
	}
	
}
