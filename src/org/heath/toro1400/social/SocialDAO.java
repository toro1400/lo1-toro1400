package org.heath.toro1400.social;

import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.heath.toro1400.social.model.Comment;
import org.heath.toro1400.social.model.Event;
import org.heath.toro1400.social.model.User;

public class SocialDAO {
	
	private EntityManagerFactory entityManagerFactory;
	
	public SocialDAO(EntityManagerFactory emf) {
		this.entityManagerFactory = emf;
	}
	
	public void persistAll(Set<User> users, Set<Event> events, Set<Comment> comments) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction transaction = entityManager.getTransaction();
		
		try {
			transaction.begin();
			for (User u : users)
				entityManager.persist(u);
			for (Event e : events)
				entityManager.persist(e);
			for (Comment c : comments)
				entityManager.persist(c);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		}
	}
}
